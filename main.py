import sys  # read config from args
import json
import time
import logging
import unittest
import test


from slack_parser import SlackParser
from msgraph import MSGraphApi

# Optional logging
logging.basicConfig(level=logging.INFO)
LIMIT_THROUGHPUT = False

if __name__ == "__main__":

    # run the tests first
    suite = unittest.TestLoader().loadTestsFromModule(test)
    result = unittest.TextTestRunner(verbosity=0).run(suite)
    if result.failures != []:
        logging.error("1 or more tests failed. Exiting with code 1.")
        sys.exit(1)

    config = json.load(open(sys.argv[1]))

    # msapi = MSGraphApi(config).getAuth() # "app flow"
    msapi = MSGraphApi(config, flow="obo").getAuth()

    if msapi is not None:
        # connection acquired, begin doing stuff

        # Example Calls
        teamId = msapi.getTeamByName(config["teamname"])
        channelId = msapi.getChatByNameAndTeamId(teamId, config["channelname"])

        # graph_data = msapi.getUsers().json()
        # graph_data = msapi.getChatByTeamAndId(teamId, channelId).json()
        # graph_data = msapi.writeChatMessageByTeamAndId(
        #        teamId=teamId,
        #        channelId=channelId,
        #        payload={ "body" : { "content" : "hello world" } }
        # ).json()

        # print("Graph API call result: ")
        # print(json.dumps(graph_data, indent=2))
        # sys.exit(0)

        sp = SlackParser(
            "resources/channels/devops", "resources/users.json", ["text", "ts", "name"]
        ).buildUsersLibrary()
        if sp.usersLib is {}:
            logging.error("Failed to read users. Shutting down with code 1.")
            sys.exit(1)

        stats_mssg_counter = 1
        logging.info("LIMIT_THROUGHPUT: " + str(LIMIT_THROUGHPUT))

        # load 1 json file at a time in memory
        for slackDay in sp.getSortedMessageFiles():
            # parse and write 1 json message at a time
            for message in sp.getMessages(slackDay):
                reduced_message = sp.getKeyValues(message)
                if reduced_message is not None:
                    teams_mssg = msapi.buildTeamsMessage(reduced_message)
                    graph_data = msapi.writeChatMessageByTeamAndId(
                        teamId=teamId, channelId=channelId, payload=teams_mssg
                    )
                    if graph_data.status_code != 201:
                        logging.error("failed to write to Teams")
                        logging.error(teams_mssg)
                        logging.error(graph_data.json())
                    else:
                        logging.info("success")
                stats_mssg_counter += 1

                # control the throughput
                if LIMIT_THROUGHPUT:
                    if stats_mssg_counter % 10 == 0:
                        time.sleep(10)
                    else:
                        time.sleep(1)

    else:
        logging.error(
            "Failed to Authenticate with Ms Graph. Shutting down with code 1."
        )
        sys.exit(1)
