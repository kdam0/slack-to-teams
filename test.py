import unittest

from msgraph import MSGraphApi
from slack_parser import SlackParser


class Test(unittest.TestCase):
    """
    The basic class that inherits unittest.TestCase
    """

    msapi = MSGraphApi()
    sp = SlackParser()

    # test case function to check the MSGraphApi.insertCodeBlocksInMessage function
    def test_0_insert_block_quotes(self):
        input_string = "sdfsdf"
        expected = "sdfsdf"
        actual = self.msapi.insertCodeBlocksInMessage(input_string)
        self.assertEqual(expected, actual)

    def test_1_insert_block_quotes(self):
        input_string = "``` CODE1 ```"
        expected = "<blockquote> CODE1 </blockquote>"
        actual = self.msapi.insertCodeBlocksInMessage(input_string)
        self.assertEqual(expected, actual)

    def test_2_insert_block_quotes(self):
        input_string = " there and ``` CODE 1 ``` asdfasdf ``` CODE 2 ``` and then again ``` CODE 3 ``` end."
        expected = " there and <blockquote> CODE 1 </blockquote> asdfasdf <blockquote> CODE 2 </blockquote> and then again <blockquote> CODE 3 </blockquote> end."
        actual = self.msapi.insertCodeBlocksInMessage(input_string)
        self.assertEqual(expected, actual)

    def test_3_insert_long_block_quotes(self):
        input_string = """<@U5F956SJH>  a long running query on the master from job01.....job-consumer-2 logs showing\n```&gt;1|ERROR|2020-03-17 15:50:00,018-0400|org.springframework.jms.listener.DefaultMessageListenerContainer#0-3|GenerateEmailNotificationTemplate:106|Unexpected error in processing.  Abending.\norg.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.exceptions.PersistenceException:\n### Error querying database.  Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications\n### Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications\n        at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:77)\n        at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\n        at com.sun.proxy.$Proxy49.selectList(Unknown Source)\n        at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:230)\n        at com.acuityads.persistence.dao.SelectQuery.selectList(SelectQuery.java:74)```\n
        """
        expected = """<@U5F956SJH>  a long running query on the master from job01.....job-consumer-2 logs showing\n<blockquote>&gt;1|ERROR|2020-03-17 15:50:00,018-0400|org.springframework.jms.listener.DefaultMessageListenerContainer#0-3|GenerateEmailNotificationTemplate:106|Unexpected error in processing.  Abending.\norg.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.exceptions.PersistenceException:\n### Error querying database.  Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications\n### Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications\n        at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:77)\n        at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\n        at com.sun.proxy.$Proxy49.selectList(Unknown Source)\n        at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:230)\n        at com.acuityads.persistence.dao.SelectQuery.selectList(SelectQuery.java:74)</blockquote>\n
        """

        actual = self.msapi.insertCodeBlocksInMessage(input_string)
        self.assertEqual(expected, actual)

    def test_4_insert_block_quotes(self):
        input_string = """<@U5F956SJH>  a long running query on the master from job01.....job-consumer-2 logs showing<p>```&gt;1|ERROR|2020-03-17 15:50:00,018-0400|org.springframework.jms.listener.DefaultMessageListenerContainer#0-3|GenerateEmailNotificationTemplate:106|Unexpected error in processing.  Abending.</p>org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.exceptions.PersistenceException:<p>### Error querying database.  Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications</p>### Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications<p>        at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:77)</p>        at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)<p>        at com.sun.proxy.$Proxy49.selectList(Unknown Source)</p>        at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:230)<p>        at com.acuityads.persistence.dao.SelectQuery.selectList(SelectQuery.java:74)```</p>
        """
        expected = """<@U5F956SJH>  a long running query on the master from job01.....job-consumer-2 logs showing<p><blockquote>&gt;1|ERROR|2020-03-17 15:50:00,018-0400|org.springframework.jms.listener.DefaultMessageListenerContainer#0-3|GenerateEmailNotificationTemplate:106|Unexpected error in processing.  Abending.</p>org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.exceptions.PersistenceException:<p>### Error querying database.  Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications</p>### Cause: java.lang.IllegalArgumentException: Mapped Statements collection does not contain value for com.acuityads.notification.dao.NotificationMapper.findNotifications<p>        at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:77)</p>        at org.mybatis.spring.SqlSessionTemplate$SqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)<p>        at com.sun.proxy.$Proxy49.selectList(Unknown Source)</p>        at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:230)<p>        at com.acuityads.persistence.dao.SelectQuery.selectList(SelectQuery.java:74)</blockquote></p>
        """
        actual = self.msapi.insertCodeBlocksInMessage(input_string)
        self.assertEqual(expected, actual)

    def test_5_parse_user_ids(self):
        input_string = "a long history of thisg"
        expected = []
        actual = self.sp.getUserIdsFromMessage(input_string)
        self.assertEqual(expected, actual)

    def test_6_parse_user_ids(self):
        input_string = "<@U32498ksjl> a long history of thisg"
        expected = ["U32498ksjl"]
        actual = self.sp.getUserIdsFromMessage(input_string)
        self.assertEqual(expected, actual)

    def test_7_parse_user_ids(self):
        input_string = " what <p> <@U32498ksjl> a long history </p> of thisg <@kumar12>"
        expected = ["U32498ksjl", "kumar12"]
        actual = self.sp.getUserIdsFromMessage(input_string)
        self.assertEqual(expected, actual)

    def test_8_get_user_name_from_user_ids(self):
        usersDict = {"U32498ksjl": "kd1", "UBLAH": "kd2"}
        test_input = ["U32498ksjl"]
        expected = {"U32498ksjl": "kd1"}
        actual = self.sp.getUserNamesFromIds(usersDict, test_input)
        self.assertEqual(expected, actual)

    def test_9_get_user_name_from_user_ids(self):
        usersDict = {"U32498ksjl": "kd1", "UBLAH": "kd2"}
        test_input = ["U32498ksjl", "UBLAH"]
        expected = {"U32498ksjl": "kd1", "UBLAH": "kd2"}
        actual = self.sp.getUserNamesFromIds(usersDict, test_input)
        self.assertEqual(expected, actual)

    def test_10_replace_userids_with_names(self):
        usersDict = {"U32498ksjl": "kd1"}
        input_string = "<@U32498ksjl> a long history of thisg"
        expected = "<i style='color: #B18904;'>@kd1</i> a long history of thisg"
        actual = self.sp.replaceMessageUserIdsWithNames(usersDict, input_string)
        self.assertEqual(expected, actual)

    def test_11_replace_userids_with_names(self):
        usersDict = {"U32498ksjl": "kd1", "UBLAH": "kd2"}
        input_string = "<@U32498ksjl> a long history of thisg from <@UBLAH> and this <@UBLAH> again."
        expected = "<i style='color: #B18904;'>@kd1</i> a long history of thisg from <i style='color: #B18904;'>@kd2</i> and this <i style='color: #B18904;'>@kd2</i> again."
        actual = self.sp.replaceMessageUserIdsWithNames(usersDict, input_string)
        self.assertEqual(expected, actual)


if __name__ == "__main__":
    unittest.main()
